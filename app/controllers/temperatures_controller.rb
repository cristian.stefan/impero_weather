class TemperaturesController < ApplicationController
  http_basic_authenticate_with name: Rails.application.config.basic_auth_user,
                               password: Rails.application.config.basic_auth_pass,
                               only: [:edit, :update]

  def show
    @temperature_status = TemperatureManager.status(params[:q]) if params[:q]
  end

  def edit; end

  def update
    if TemperatureManager.update_bounds(params[:cold_tempmerature_upper], params[:warm_tempmerature_upper].to_i)
      redirect_to action: :show
    else
      flash.now[:error] = 'cold > warm'
      render :edit
    end
  end
end
