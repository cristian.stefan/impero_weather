# RailsSettings Model
class Setting < RailsSettings::Base
  field :cold_tempmerature_upper, type: :integer, default: 20, validates: { presence: true }
  field :warm_tempmerature_upper, type: :integer, default: 30, validates: { presence: true }
end
