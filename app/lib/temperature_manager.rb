class TemperatureManager
  class << self
    def update_bounds(lower_bound, upper_bound)
      return false if lower_bound.to_i >= upper_bound.to_i

      Setting.cold_tempmerature_upper = lower_bound
      Setting.warm_tempmerature_upper = upper_bound
    end

    def status(postcode)
      temperature = TemperatureApi.call(postcode).result
      return 'cold' if temperature <= Setting.cold_tempmerature_upper
      return 'warm' if temperature <= Setting.warm_tempmerature_upper

      'hot'
    rescue AppErrors::WeatherApi => e
      e.message
    rescue StandardError
      Rails.logger.error('ERROR: TemperatureApi.call failed to fetch api temperature')
      'Data unavailable'
    end
  end
end
