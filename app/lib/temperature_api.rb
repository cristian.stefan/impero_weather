class TemperatureApi < Patterns::Service
  BASE_URL = 'http://api.weatherapi.com/v1'.freeze
  API_KEY = Rails.application.config.weather_api_key.freeze

  # pass a postcode to retrieve data of that specific location
  def initialize(postcode)
    @postcode = postcode
  end

  # returns the temperature for the postcode location
  def call
    raise AppErrors::WeatherApi::UnpermittedZone if country && country != 'UK'

    temperature
  end

  private

  attr_reader :postcode

  # get temperature from api response
  def temperature
    @temperature ||= api_data_json.dig('forecast', 'forecastday', 0, 'day', 'maxtemp_c')
  end

  def country
    @country ||= api_data_json.dig('location', 'country')
  end

  def api_data_json
    @api_data_json ||= JSON.parse(api_data)
  end

  # fetch api data from weatherapi.com
  def api_data
    Faraday.get(api_url).body
  end

  # build api url. it usses the 0 day for data
  def api_url
    "#{BASE_URL}/forecast.json?key=#{API_KEY}&q=#{postcode}&days=0"
  end
end
