module AppErrors
  class WeatherApi < AppError
    class UnpermittedZone < WeatherApi
      def message
        'Zone unpermitted'
      end
    end
  end
end
