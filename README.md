## Inatallation Notes

Install ruby and bundle install
* Ruby version
ruby-3.1.2

* Bundler
BUNDLED WITH 2.3.7

## Run migrations
rails db:create
rails db:migrate

## Start server
rails s

## Use app
Go to [localhost:3000](localhost:3000)
