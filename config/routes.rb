Rails.application.routes.draw do
  root 'temperatures#show'
  namespace :temperatures do
    get :show
    get :edit
    patch :update
  end
end
