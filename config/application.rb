require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ImperoWeather
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # basic auth
    config.basic_auth_user = ENV.fetch 'BASIC_AUTH_USER'
    config.basic_auth_pass = ENV.fetch 'BASIC_AUTH_PASS'

    # weather api
    config.weather_api_key = ENV.fetch 'WEATHER_API_KEY'
  end
end
