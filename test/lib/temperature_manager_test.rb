require 'test_helper'

class TemperatureManagerTest < ActiveSupport::TestCase
  test 'returns false on #upper_bounds invalid values' do
    assert_not  TemperatureManager.update_bounds(2, 1)
  end

  test 'should update values for temp limits' do
    temp_cold = Setting.cold_tempmerature_upper
    temp_warm = Setting.warm_tempmerature_upper
    TemperatureManager.update_bounds(temp_cold + 1, temp_warm + 1)
    assert_equal temp_cold + 1, Setting.cold_tempmerature_upper
    assert_equal temp_warm + 1, Setting.warm_tempmerature_upper
  end

  test 'returns correct status' do
    # TemperatureApi = Minitest::Mock.new
    # TemperatureApi.expect :call, OpenStruct.new({ result: 20 })
    TemperatureManager.update_bounds(20, 22)
    TemperatureApi.stub :call, OpenStruct.new({ result: 19 }) do
      assert TemperatureManager.status('cone_code') == 'cold'
    end
    TemperatureApi.stub :call, OpenStruct.new({ result: 20 }) do
      assert TemperatureManager.status('cone_code') == 'cold'
    end
    TemperatureApi.stub :call, OpenStruct.new({ result: 21 }) do
      assert TemperatureManager.status('cone_code') == 'warm'
    end
    TemperatureApi.stub :call, OpenStruct.new({ result: 22 }) do
      assert TemperatureManager.status('cone_code') == 'warm'
    end
    TemperatureApi.stub :call, OpenStruct.new({ result: 23 }) do
      assert TemperatureManager.status('cone_code') == 'hot'
    end
  end
end
