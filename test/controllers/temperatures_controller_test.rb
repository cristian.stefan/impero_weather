require 'test_helper'

class TemperaturesControllerTest < ActionDispatch::IntegrationTest
  def basic_auth_headers
    username = ENV.fetch 'BASIC_AUTH_USER'
    password = ENV.fetch 'BASIC_AUTH_PASS'
    { HTTP_AUTHORIZATION: ActionController::HttpAuthentication::Basic.encode_credentials(username, password) }
  end

  test 'should #show success' do
    get temperatures_show_url
    assert_response :success
  end

  test 'should #update success' do
    patch temperatures_update_url, headers: basic_auth_headers
    assert_response :success
  end

  test 'should #edit success' do
    get temperatures_edit_url, headers: basic_auth_headers
    assert_response :success
  end

  test 'should #update values for temp limits' do
    temp_cold = Setting.cold_tempmerature_upper
    temp_warm = Setting.warm_tempmerature_upper
    patch temperatures_update_url,
          headers: basic_auth_headers,
          params: { cold_tempmerature_upper: temp_cold + 1, warm_tempmerature_upper: temp_warm + 1 }
    assert_equal temp_cold + 1, Setting.cold_tempmerature_upper
    assert_equal temp_warm + 1, Setting.warm_tempmerature_upper
  end
end
