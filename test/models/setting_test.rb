require 'test_helper'

class SettingTest < ActiveSupport::TestCase
  test 'should not permit blank on cold_tempmerature_upper and warm_tempmerature_upper' do
    assert_raises(ActiveRecord::RecordInvalid) do
      Setting.cold_tempmerature_upper = nil
      Setting.warm_tempmerature_upper = nil
    end
  end
end
